const dbCreds = {
  user: "admindb",
  database: "denoapi",
  password: "adminpass",
  hostname: "localhost",
  port: 54322
}

export { dbCreds }
